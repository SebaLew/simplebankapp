package bankapplication;

import java.util.LinkedList;
import java.util.List;

public class BankAccountApp {
    public static void main(String[] args) {
        String file = "D:\\wypociny\\java projects udemy\\NewBankAccounts.csv";
        /*

        Checking chkacc1=new Checking("Tom", "123456789", 1500);
        Savings savacc1 = new Savings("Seb Lev", "987654321", 2500);

        savacc1.compound();

        chkacc1.showInfo();
        System.out.println("**********************************");
        savacc1.showInfo();

 */
        List<Account> accounts = new LinkedList<Account>();

        //read a csv file create new account based on that data
        List<String[]> newAccountHolders = utilities.CSV.read(file);
        for (String[] accountHolder : newAccountHolders) {
            String name = accountHolder[0];
            String sSN = accountHolder[1];
            String accountType = accountHolder [2];
            double initDeposit = (Double.parseDouble(accountHolder [3]));
            System.out.println(name + " " + sSN + " " + accountType + " $" + initDeposit);
            if (accountType.equals("Savings")) {
                System.out.println("open a savings account");
                accounts.add(new Savings(name, sSN, initDeposit));
                }
            else if (accountType.equals("Checking")) {
                System.out.println("open a chacking account");
                accounts.add(new Checking(name, sSN, initDeposit));
            }
            else {
                System.out.println("Error reading account type");
            }
        }
        for (Account acc : accounts) {
            System.out.println("********");
            acc.showInfo();
        }
    }
}
