package bankapplication;

public class Checking extends  Account {

    //list properties specific to checking accounts
    private int debitCardNumber;
    private int debitCardPIN;

    //constructor to initialize checking accounts

    public Checking(String name, String sSN, double initDeposit) {
        super(name, sSN, initDeposit);
        accountNumber= "2" + accountNumber;
        setDebitCard();
    }

    public void setRate() {
        rate=getBaseRate()*0.15;
    }

    //list any methods specific to the checking account
    private void setDebitCard() {
        debitCardNumber =(int) (Math.random() * Math.pow(10,12));
        debitCardPIN = (int) (Math.random() * Math.pow(10,4));;
    }

    public void showInfo() {
        System.out.println("ACCOUNT TYPE: Checking");
        super.showInfo();
        System.out.println("Your checking account features:" +
                "\n Debit Card Number " + debitCardNumber +
                "\n Debit Card PIN " + debitCardPIN
        );
    }



    //list any methods specific to checking account

    //
}
