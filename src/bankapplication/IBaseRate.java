package bankapplication;

public interface IBaseRate {

    //method that returns IBaseRate
    default double getBaseRate() {
        return 2.5;
    }
}
